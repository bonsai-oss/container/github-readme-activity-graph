FROM debian:bullseye-slim as downloader
RUN apt update && apt install git -y
WORKDIR /download
RUN git clone https://github.com/Ashutosh00710/github-readme-activity-graph.git

FROM node:22.12.0
WORKDIR /app
COPY --from=downloader /download/github-readme-activity-graph .
RUN npm install

EXPOSE 5100
ENTRYPOINT ["npx", "ts-node", "src/main.ts"]